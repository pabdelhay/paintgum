'use strict';

/*
 * https://github.com/sitegui/nodejs-websocket
 */

var WebSocketServer = require("ws").Server
var http = require("http")
var fs = require("fs")
var path = require('path')
var express = require('express')

// HTTP SERVER
var app = express();
var port = process.env.PORT || 5000;
app.use('/assets/js', express.static(path.join(__dirname, 'public', 'assets', 'js')));
app.use('/assets/css', express.static(path.join(__dirname, 'public', 'assets', 'css')));
app.use('/assets/img', express.static(path.join(__dirname, 'public', 'assets', 'img')));
app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

var server = http.createServer(app)
server.listen(port)
console.log("Listening on " + port + "...");


// GAME CONFIGS
var gameWidth = 600;
var gameHeight = 480;
var actual_team = 1;
var team_1_initial_x = 10, team_1_initial_y = gameHeight/2;
var team_2_initial_x = gameWidth-10, team_2_initial_y = gameHeight/2;
var playerWidth = 30;
var playerHeight = 30;
var respawn_time = 5000; // miliseconds

var dead_players = {};
var players = {};
var Player = class {
	constructor(id, team) {
		this.id = id;
		this.team = team;
		this.angle = 0;
		this.dead = false;
		
		this.kills = 0;
		this.deaths = 0;
		
		this.setInitialPos();
	}
	
	send(){
		broadcast(JSON.stringify({'action': 'update', 'element': 'player', 'object': this}));
	}
	
	setInitialPos(){
		this.x = this.team == 1 ? team_1_initial_x : team_2_initial_x;
		this.y = this.team == 1 ? team_1_initial_y : team_2_initial_y;
	}
	
	getBounds(){
		var x1 = this.x - playerWidth/2;
		var y1 = this.y - playerHeight/2;
		
		var x2 = this.x + playerWidth/2;
		var y2 = this.y - playerHeight/2;
		
		var x3 = this.x + playerWidth/2;
		var y3 = this.y + playerHeight/2;
		
		var x4 = this.x - playerWidth/2;
		var y4 = this.y + playerHeight/2;
		
		return [[x1, y1], [x2, y2], [x3, y3], [x4, y4]];
	}
	
	inBounds(x, y){
		var bounds = this.getBounds();
		var x1 = bounds[0][0];
		var y1 = bounds[0][1];
		var x2 = bounds[1][0];
		var y2 = bounds[1][1];
		var x3 = bounds[2][0];
		var y3 = bounds[2][1];
		var x4 = bounds[3][0];
		var y4 = bounds[3][1];
		if((x >= x1 && x <= x2) && (y >= y1 && y <= y4))
			return true;
		return false;
	}
	
	kill(){
		this.kills += 1;
		this.send();
	}
	die(){
		this.deaths += 1;
		this.dead = true;
		this.send();
		this.setInitialPos();
		
		var self = this;
		setTimeout(function(){
			self.dead = false;
			self.send();
		}, respawn_time);
	}
	
	
}
var shots = {};
var Shot = class {
	constructor(id, player_id, x, y, target_x, target_y) {
		this.id = id;
		this.player_id = player_id;
		this.x = x;
		this.y = y;
		this.target_x = target_x;
		this.target_y = target_y;
	}
}


var wss = new WebSocketServer({server: server})
console.log("WebSocket server created")

wss.on("connection", function(connection) {
	connection.player = null
	connection.on("message", function (str, flags) {
		var data = JSON.parse(str); // {'action': ('new_player', 'update'), 'element': ('player', 'shot'), 'object': (Player, Shot)}
		switch(data.action){
			case('new_player'):
				var nickname = data.object;
				if (connection.player === null) {					
					connection.player = new Player(nickname, actual_team);
					actual_team = (actual_team == 1 ? 2 : 1);
					
					players[connection.player.id] = connection.player;
					var players_list = Object.keys(players).map(key => players[key]);
					connection.send(JSON.stringify({'action': 'init', 'players': players_list}));
				}
				broadcast(JSON.stringify({'action': 'update', 'element': 'player', 'object': connection.player}));
				break;
			case('update'):
				if(data.element == 'player'){
					connection.player.x = data.object.x;
					connection.player.y = data.object.y;
					connection.player.angle = data.object.angle;
					broadcast(JSON.stringify({'action': 'update', 'element': 'player', 'object': connection.player}));
				} else if(data.element == 'shot'){
					var object = data.object;
					if(shots[object.id]){
						// change shot position.
						shots[object.id].x = object.x;
						shots[object.id].y = object.y;
					} else {
						// new shot.
						shots[object.id] = new Shot(object.id, connection.player.id, object.x, object.y, object.target_x, object.target_y);
					}
					broadcast(JSON.stringify({'action': 'update', 'element': 'shot', 'object': shots[object.id]}));
					
					// Check for kill.
					for (var key in players) {
						if(players[key].id == connection.player.id)
							continue;
						if(players[key].dead)
							continue;
						
						if(players[key].inBounds(object.x, object.y)){
							connection.player.kill();
							players[key].die();
							
							broadcast(JSON.stringify({'action': 'kill', 'player_dead_id': players[key].id, 'player_killer_id': connection.player.id}));
							broadcast(JSON.stringify({'action': 'remove', 'element': 'shot', 'object': shots[object.id]}));
							
							delete shots[object.id];
						}
					}
				}
				break;
			case('remove'):
				if(data.element == 'shot'){
					var object = data.object;
					broadcast(JSON.stringify({'action': 'remove', 'element': 'shot', 'object': shots[object.id]}));
					delete shots[object.id];
				}
				break;
		}
		
	})
	connection.on("close", function () {
		broadcast(JSON.stringify({'action': 'remove', 'element': 'player', 'object': connection.player}));
		delete players[connection.player.id];
		connection.player = null;
	})
})

function broadcast(str) {
	wss.clients.forEach(function each(client) {
		client.send(str);
	});
}
